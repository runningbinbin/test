FROM cl00e9ment/node.js-builder 

RUN  mkdir -p /home/user/myapp

ADD hello_world.cpp /home/user/myapp

WORKDIR /home/user/myapp

RUN  c++ hello_world.cpp -o hello_world

CMD ["./hello_world"]

